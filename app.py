# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.offline
import plotly.graph_objs as go
import math
from math import exp
import numpy as np
from dash.dependencies import Input, Output, State
from plotly.offline import download_plotlyjs, plot

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
data = pd.read_excel('data-reliability.xlsx')

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.css.config.serve_locally = True
app.scripts.config.serve_locally = True
app.title = "Change Calculator"

server = app.server

data['measure'] = data[['Measure','Subset']].apply(lambda x: ' '.join(x), axis=1)

all_measures = pd.Series.unique(data['measure'])

sigtableCols = ['First Score', 'Second Score', 'A','C','B','Difference',
        'Threshold','Significance']

#print(all_measures)

# Set some default values for the figure
fig_default = {
    'data' : [
        {
           'x' : [0],
           'y' : [0],
           'name' : 'default_trace',
           'mode' : 'markers'
        }
    ],
    'layout': {
        'clickmode' : 'event+select'
    }
}


app.layout = html.Div([
    html.H1('Measuring Clinically Significant Change'),
    html.P('This interactive tool calculates whether change in one of the' +
    ' included measures is clinically significant based on the models described' +
    ' by Jacobson and Truax (1991).'),
    #html.H2('Instructions'),
    #html.P('more info to come'),
    html.Div([
        html.Div([
            html.Label('Select a Measure'),
            dcc.Dropdown(
                id = 'measure-selector',
                options = [{'value': i, 'label': i} for i in all_measures]
                #value = all_measures[0],
                #className = "measures-radio",
                #inputClassName = "measure-radio"
                )
            ]),
        html.Div([
            html.Label('Age Range'),
            dcc.RadioItems(id = 'age-selector')
            ], style = {'display': 'inline-block', 'margin':'10px','vertical-align':'top'}),
        html.Div([
            html.Label('Gender'),
            dcc.RadioItems(id = 'sex-selector')
            ], style = {'display': 'inline-block', 'margin':'10px', 'vertical-align':'top'}),
        html.Div([
            html.Label('Score Type'),
            dcc.RadioItems(id = 'scoretype-selector')
            ], style = {'display': 'inline-block', 'margin':'10px', 'vertical-align':'top'}),
        html.Div([
            html.Label('Input Scores'),
            dcc.Input(id='score1',
                placeholder="Score 1",
                type='text', style={'width':'75px'}),
            dcc.Input(id='score2',
                placeholder="Score 2",
                type='text', style={'width':'75px'}),
            html.Div([
                html.Label('Select Significance Level'),
                dcc.RadioItems(id='siglev',
                    options=[{'value': i, 'label': i} for i in ['90','95']])
                ]),
            html.Div([
                html.Button(id='plot-button', n_clicks=0, children="Plot Graph")
            ]),
            html.Button(id='table-button', n_clicks=0, children="Build Table", style = {'float':'left'})
            ])
    ], style = {'float':'left','width':'25%'}),
    html.Div([
        dcc.Graph(id='fig',figure = fig_default),
        html.Table(
                    #header
            [html.Tr([html.Th(col) for col in sigtableCols])] +
                    #body
            [html.Tr(id="table-vals")],
            style = {'margin':'auto'})
    ], style = {"float":"right", "width":"75%"})
])

@app.callback(
        Output('age-selector','options'),
        [Input('measure-selector','value')])
def set_age(selected_measure):
    df = data.loc[data['measure'] == selected_measure]
    return([{'value': i, 'label': i} for i in pd.Series.unique(df['AgeRange'])])

@app.callback(
        Output('sex-selector','options'),
        [Input('age-selector','value'),
        Input('measure-selector','value')])
def set_sex(selected_age, selected_measure):
    df = data.loc[(data['measure'] == selected_measure) & (data['AgeRange'] == selected_age)]
    return([{'value': i, 'label': i} for i in pd.Series.unique(df['Gender'])])

@app.callback(
        Output('scoretype-selector','options'),
        [Input('age-selector','value'),
        Input('measure-selector','value'),
        Input('sex-selector','value')])
def set_scoretype(selected_age, selected_measure,selected_sex):
    df = data.loc[(data['measure'] == selected_measure) &
            (data['AgeRange'] == selected_age) &
            (data['Gender'] == selected_sex)]
    return([{'value': i, 'label': i} for i in pd.Series.unique(df['ScoreType'])])

def dnorm(xdat,mu,sigma):
    return([exp((-(x-mu)**2)/(2*sigma**2)) / (sigma * math.sqrt(2*math.pi)) for x in xdat])

@app.callback(
        Output('table-vals','children'),
        [Input('table-button', 'n_clicks')],
        [State('score1','value'),
        State('score2','value'),
        State('siglev','value'),
        State('measure-selector','value'),
        State('sex-selector','value'),
        State('scoretype-selector','value'),
        State('age-selector','value')])
def set_table(nclicks, score1, score2, siglev, measure, sex, scoretype, age):
    """
    TODO: add try/except logic to handle exceptions
    """
    if score1 is None or score2 is None:
        return([])

    df = data.loc[(data['measure'] == measure) &
            (data['AgeRange'] == age) &
            (data['Gender'] == sex) &
            (data['ScoreType'] == scoretype)]

    A = round(df['A'].iloc[0], 1)
    B = round(df['B'].iloc[0], 1)
    C = round(df['C'].iloc[0], 1)

    diff = abs(round(float(score2) - float(score1), 1))
    thresh = ''
    if siglev == '90':
       thresh = round(df['RC90'].iloc[0], 1)
    if siglev == '95':
        thresh = round(df['RC95'].iloc[0], 1)

    sig = ''
    if diff <= thresh:
        sig = 'Not Significant'
    if diff > thresh:
        sig = 'Significant'

    row = [score1, score2, A, B, C, diff, thresh, sig]

    return([html.Td(r) for r in row])


@app.callback(
        Output('fig','figure'),
        [Input('plot-button', 'n_clicks')],
        [State('age-selector','value'),
        State('measure-selector','value'),
        State('sex-selector','value'),
        State('scoretype-selector','value'),
        State('score1','value'),
        State('score2','value')])
def set_figdata(nclicks, selected_age, selected_measure,selected_sex,selected_score,score1,score2):
    if selected_score is None:
        return(None)


    df = data.loc[(data['measure'] == selected_measure) &
            (data['AgeRange'] == selected_age) &
            (data['Gender'] == selected_sex) &
            (data['ScoreType'] == selected_score)]

    mu_clin = df['M_clin'].iloc[0]
    sd_clin = df['SD_clin'].iloc[0]

    mu_nc = df['M_nc'].iloc[0]
    sd_nc = df['SD_nc'].iloc[0]

    xdat = np.linspace(0,100,101)
    
    d = {
            'xclin': xdat,
            #'yclin': dnorm(linsp, mu_clin, sd_clin),
            'yclin': np.apply_along_axis(dnorm,0,xdat,mu = mu_clin,sigma = sd_clin),
            'xnc'  : xdat,
            #'ync'  : dnorm(linsp, mu_nc, sd_nc)
            'ync'  : np.apply_along_axis(dnorm,0,xdat,mu=mu_nc,sigma=sd_nc)
        }
    dd = pd.DataFrame(data = d)

    #dd = dd[(dd['yclin'] > .1) | (dd['ync'] > .1)]
    y1_1 = max([max(dd['yclin']),max(dd['ync'])])
    y1_2 = y1_1
    try:
        s1 = float(score1)
    except (ValueError, TypeError):
        y1_1 = 0
        s1 = 0

    try:
        s2 = float(score2)
    except (ValueError, TypeError):
        y1_2 = 0
        s2 = 0

    traces = []

    traces.append(go.Scatter(
        x=dd['xclin'],
        y=dd['yclin'],
        name='Clinical',
        mode='lines'
        ))

    traces.append(go.Scatter(
        x=dd['xnc'],
        y=dd['ync'],
        name='Non-Clinical',
        mode='lines'
        ))

    return({
        'data' : traces,
        'layout': go.Layout(
            xaxis={'title' : '{} score'.format(selected_measure)},
            yaxis={'title' : 'Proportion of Population'},
            shapes=[
                {'type':'line','x0':s1,'y0':0, 'x1':s1,'y1':y1_1},
                {'type':'line','x0':s2,'y0':0, 'x1':s2,'y1':y1_2}
                ],
            legend={'x':1,'y':1}
            )
        })

if __name__ == '__main__':
    app.run_server(debug=True)
"""
TODO:
[x] Dropdown menu does not render correctly on Chrome
"""
