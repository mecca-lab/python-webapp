# Data visualization: Measuring Clinically Significant Change

This app uses Dash to render visualizations of clinical and nonclincal score distributions for a variety of common measures. Dash runs on top of Flask, and this app is meant to be deployed on Azure App Service on Linux.

## TODO
* [ ]  Center score input and table below figure
* [ ]  Add A, B, C makers to the figure
* [ ]  Make stylesheet local for greater style customization
* [ ]  Change background color to something not white (blue?)